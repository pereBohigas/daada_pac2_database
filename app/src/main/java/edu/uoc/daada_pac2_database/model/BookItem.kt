package edu.uoc.daada_pac2_database.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity(tableName = "book_table")
data class BookItem (

    //@PrimaryKey(autoGenerate = true)
    @PrimaryKey
    @NonNull
    val identifier: Int,
    @NonNull
    val title: String,
    @NonNull
    val author: String,
    @ColumnInfo(name = "publication_date")
    val publicationDate: String,
    val description: String,
    @ColumnInfo(name = "url_image")
    val imageURL: String

) {

    override fun toString(): String {
        return "[ID: ${identifier}, TITLE: ${title}, AUTHOR: ${author}, PUBLICATION DATE: ${publicationDate}]"
    }

    override fun equals(otherBook: Any?): Boolean {
        if (otherBook is BookItem) {
            if (this.title.equals(otherBook.title) && this.author.equals(otherBook.author) && this.publicationDate.equals(publicationDate)) {
                return true
            }
        }
        return false
    }
}
